@extends('layouts.layout')
  
@section('content')
<body>
<div class="container">
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Publicar</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('layouts.index') }}"> Atras</a>
            </div>
        </div>
    </div>

<form action="{{ route('layouts.store') }}" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Perfil:</strong>
                <input type="text" name="title" class="form-control" placeholder="Titulo de la publicación">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Contenido:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Enter Description"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Crear</button>
        </div>
    </div>
   
    </form>
</div>
</body>
@endsection