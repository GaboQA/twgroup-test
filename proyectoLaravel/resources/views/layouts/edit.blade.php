@extends('layouts.layout')
   
@section('content')
<body>
<div class="container">
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Actualizar publicación</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('layouts.index') }}"> Atras</a>
            </div>
        </div>
    </div>
 
    <form action="{{ route('layouts.update',$layout->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Titulo:</strong>
                    <input type="text" name="title" value="{{ $layout->title }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Contenido:</strong>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Detail">{{ $layout->description }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
   
    </form>
</div>
</body>
    
@endsection