@extends('layouts.layout')
  
@section('content')
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Comentarios</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('layouts.index') }}"> Atras</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Titulo:</strong>
                {{ $layout->title }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Contenido:</strong>
                {{ $layout->description }}  
            </div>
        </div>
        <div class="container">
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ url('/createcomment') }}"> Nueva comentario</a>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Comentarios</th>
        </tr>
        @foreach ($comments as $key => $value)
        <tr>
            <td>{{ $value->content }}</td>
        </tr>
        @endforeach
    </table>  
</div>
    </div>
    </div>
</body>
@endsection