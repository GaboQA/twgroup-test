@extends('layouts.app')
 
@section('content')
<body>
    
<div class="container">
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('layouts.create') }}"> Nueva publicación</a>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Titulo</th>
            <th>Publicación</th>
            <th width="280px">Acciónes</th>
        </tr>
        @foreach ($data as $key => $value)
        <tr>
            <td>{{ $value->title }}</td>
            <td>{{ \Str::limit($value->description, 100) }}</td>
            <td>
                <form action="{{ route('layouts.destroy',$value->id) }}" method="POST">   
                    <a class="btn btn-info" href="{{ route('layouts.show',$value->id) }}">Ver</a>    
                    <a class="btn btn-primary" href="{{ route('layouts.edit',$value->id) }}">Cambiar</a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  

</div>

</body>

    {!! $data->links() !!}      
@endsection