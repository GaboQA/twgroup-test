<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Publication;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;

class PublicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Publication::latest()->paginate(5);
    
        return view('layouts.index',compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
            
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.create');
    }


    public function createcomment()
    {
        return view('layouts.createcomment');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->content != null){
            $request->validate([
                'content' => 'required',
            ]);
        
            Comment::create($request->all());
            //Enviamos el correo electronico
            /*$data = array('name'=>"san jose","body"=>"Test mail");
            Mail::send('pruebaC',$data, function($message){
                $message->to('jositnquesada@gmail.com',"Artisan web") 
                ->subject('Artisan web');
                $message->from('jositnquesada@gmail.com','Gabriel');
            });*/
            return redirect()->back();
        }

        else{
            $request->validate([
                'title' => 'required',
                'description' => 'required',
            ]);
        
            Publication::create($request->all());
         
            return redirect()->route('layouts.index')
            ->with('success','Se publico con exito.');
        }
         
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Publication  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Publication $layout)
    {
        $comments = Comment::latest()->paginate(7);
    
        return view('layouts.show',compact('layout','comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Publication  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $layout)
    {
        return view('layouts.edit',compact('layout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Publication  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publication $layout)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
    
        $layout->update($request->all());
    
        return redirect()->route('layouts.index')
                        ->with('success','Se actualizo con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Publication  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publication $layout)
    {
        $layout->delete();
    
        return redirect()->route('layouts.index')
                        ->with('success','Eliminado');
    }
}