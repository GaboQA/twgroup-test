<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/prueba', [App\Http\Controllers\ProductController::class, 'index']);
Route::get('/prueba2', [App\Http\Controllers\PublicationController::class, 'show']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\PublicationController::class, 'index'])->name('home');




Route::resource('layouts', PublicationController::class);

Route::resource('comments', CommentController::class);

Route::get('/createcomment', [App\Http\Controllers\PublicationController::class, 'createcomment']);
